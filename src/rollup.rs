use actix_web::{web, App, HttpResponse, HttpServer, Result};
use ed25519_dalek::{Signature, Verifier, VerifyingKey};
use lazy_static::lazy_static;
use starling::hash_tree::HashTree;
use starling::Array;
use std::error::Error;
use std::sync::Mutex;
use simple_rollup::TransactionReq;

struct Transaction {
    signature_bytes: Vec<u8>,
    payload_bytes: Vec<u8>,
    public_key_bytes: Vec<u8>,
}

lazy_static! {
    static ref TXN_TRIE: Mutex<HashTree> = Mutex::new(HashTree::new(8).unwrap());
    static ref TRIE_ROOT: Mutex<Option<Array<32>>> = Mutex::new(None);
}

async fn handle_txn(req: web::Json<TransactionReq>) -> Result<HttpResponse> {
    let signature_bytes = bs58::decode(&req.signature)
        .into_vec()
        .map_err(|_| actix_web::error::ParseError::Incomplete)?;
    let payload_bytes = bs58::decode(&req.payload.as_bytes())
        .into_vec()
        .map_err(|_| actix_web::error::ParseError::Incomplete)?;
    let public_key_bytes = bs58::decode(&req.public_key)
        .into_vec()
        .map_err(|_| actix_web::error::ParseError::Incomplete)?;
    let txn = Transaction {
        signature_bytes,
        payload_bytes,
        public_key_bytes,
    };
    if let Ok(true) = verify_txn(&txn) {
        let proof = add_txn_to_trie(&txn);
        Ok(HttpResponse::Ok().json(proof))
    } else {
        Ok(HttpResponse::BadRequest().json("Transaction is invalid"))
    }
}

fn verify_txn(txn: &Transaction) -> Result<bool, Box<dyn Error>> {
    verify_signature(&txn)
    // in a real rollup, it also need to verify more thing: interprete the payload and determine whether it's
    // valid according to the state trie
}

fn verify_signature(txn: &Transaction) -> Result<bool, Box<dyn Error>> {
    let public_key = VerifyingKey::from_bytes(&txn.public_key_bytes.clone().try_into().unwrap())?;
    let signature = Signature::from_bytes(&txn.signature_bytes.clone().try_into().unwrap());

    if public_key.verify(&txn.payload_bytes, &signature).is_ok() {
        Ok(true)
    } else {
        Ok(false)
    }
}

fn add_txn_to_trie(txn: &Transaction) -> Vec<(Array<32>, bool)> {
    let key: Array<32> = txn.signature_bytes.clone().as_slice()[..32].try_into().unwrap();
    let value: Vec<u8> = txn.payload_bytes.clone();
    let mut tree = TXN_TRIE.lock().unwrap();
    let new_root = tree
        .insert(TRIE_ROOT.lock().unwrap().as_ref(), &mut [key], &[value])
        .unwrap();
    *TRIE_ROOT.lock().unwrap() = Some(new_root);
    tree.generate_inclusion_proof(&new_root, key).unwrap()
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().service(web::resource("/tx").route(web::post().to(handle_txn))))
        .bind("127.0.0.1:8080")?
        .run()
        .await
}
