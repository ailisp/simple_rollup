use actix_rt::System;
use awc::Client;
use serde_json;
use simple_rollup::TransactionReq;

fn main() {
    System::new().block_on(async {
        let args: Vec<String> = std::env::args().collect();
        if args.len() != 2 {
            eprintln!("Usage: {} <json_data>", args[0]);
            std::process::exit(1);
        }

        let json_data = args[1].as_str();
        let req: TransactionReq = match serde_json::from_str(json_data) {
            Ok(req) => req,
            Err(err) => {
                eprintln!("Error parsing JSON: {}", err);
                std::process::exit(1);
            }
        };

        let url = "http://localhost:8080/tx";

        let client = Client::new();

        let response = client.post(url)
            .send_json(&req)
            .await;

        match response {
            Ok(mut resp) => {
                if resp.status().is_success() {
                    println!("successfully submit transaction. response: {}", resp.json::<serde_json::Value>().await.unwrap().to_string());
                } else {
                    eprintln!("Failed to post JSON data. Status: {}", resp.status());
                }
            }
            Err(err) => {
                eprintln!("Error sending request: {}", err);
            }
        }
    });
}
