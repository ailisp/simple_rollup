use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct TransactionReq {
    pub signature: String,  // Base58-encoded signature
    pub payload: String,    // Base58-encoded data to verify
    pub public_key: String, // Base58-encoded public key
}